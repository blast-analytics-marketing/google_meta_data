class String
  def format_for_google
    base_string = self.gsub("ga:", "")
    if base_string.length == 3
      base_string[0, 1].downcase + base_string[1..-1]
    else
      base_string
    end
  end
end