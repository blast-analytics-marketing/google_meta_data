class GoogleDimensionsAndMetrics
  require 'httparty'
  # application requests should be made here https://www.googleapis.com/analytics/v3/metadata/ga/columns
  # This class does not make a 'traditional' request to a google api via Oauth2, it rather places a 
  # standard get request to the URL expressed in the GOOGLE_API_URL constant. 

  GOOGLE_API_URL = 'https://www.googleapis.com/analytics/v3/metadata/ga/columns'

  def initialize
    if @initial_response = HTTParty.get(GOOGLE_API_URL)
      @items = @initial_response["items"]
      @etag = @initial_response["etag"]
      @dimensions = {}
      @metrics = {}
      @custom_variable_dimensions = []
      @custom_variable_metrics = []
      @sql_attributes = []
      increment_and_set_sql_attributes
      set_custom_dimensions
      set_custom_metrics
      set_dimensions_and_metrics_hash_keys
      aggregate_and_cast_dimensions
      aggregate_and_cast_metrics
    else
      raise "Unable to connect to the API URL. Please ensure you are connected to the internet."
    end
  end

  # Returns current google dimensions
  #
  # @return [Hash]
  #
  def dimensions
    @dimensions
  end

  # Returns current google metrics
  #
  # @return [Hash]
  #
  def metrics
    @metrics
  end

  # Returns the current google metadata etag
  #
  # @return [String]
  #
  def etag
    @etag
  end

  # Returns the current google metadata etag
  #
  # @return [Array]
  #
  def sql_attributes
    @sql_attributes
  end

  # Writes current instances data to cache
  #
  # @return [Undefined]
  #
  def write_to_cache!
    Rails.cache.write("GA:etag", @etag, expires_in: 5.days)
    Rails.cache.write("GA:Dimensions", dimensions, expires_in: 5.days)
    Rails.cache.write("GA:Metrics", metrics, expires_in: 5.days)
    Rails.cache.write("GA:SQL_attributes", sql_attributes, expires_in: 5.days)
  end

  #
  # Class methods

  def self.check_for_new_cache
    old_etag = Rails.cache.fetch("GA:etag")

    if d_and_m = self.new
      if (old_etag.nil? || d_and_m.etag != old_etag)
        d_and_m.write_to_cache!
        d_and_m
      else
        puts "did not write to cache"
      end
    end
  end

  # Checks current cache for data, if it is not available, then it will create
  # a new instance of this class and return that data.
  #
  # @return [Hash]
  #
  def self.fetch_dimensions
    cached = Rails.cache.fetch("GA:Dimensions")
    if cached.present?
      cached
    else
      self.check_for_new_cache.try(:dimensions)
    end
  end

  def self.fetch_metrics
    cached = Rails.cache.fetch("GA:Metrics")    
    if cached.present?
      cached
    else
      self.check_for_new_cache.try(:metrics)
    end
  end

  def self.fetch_sql_attributes
    cached = Rails.cache.fetch("GA:SQL_attributes")    
    if cached.present?
      cached
    else
      self.check_for_new_cache.try(:sql_attributes)
    end
  end

  private
  
    def add_number_var_to_attribute(base_string, options={})
      array_of_attr = []

      options = {
        existing_array: nil, 
        number_of_times: 20,
        regex: "XX"
      }.merge(options)

      options[:number_of_times].times{|n| array_of_attr << base_string.gsub(options[:regex], "#{n+1}")}

      if existing_array = options[:existing_array]
        existing_array.concat(array_of_attr)
      else
        array_of_attr
      end
    end

    #
    # Attributes that we use in our sql methods are defined as an attribute 
    # that is not a string or an integer. Here we take all of the attributes
    # that meet that criteria, and put them into an instance variable, while
    # processing those that need incremental variables
    #
    # @return [Undefined]

    def increment_and_set_sql_attributes
      @items.each do |item|
        attribute = item["id"].format_for_google
        unless (item["attributes"]["dataType"] == "STRING") || (item["attributes"]["dataType"] == "INTEGER")
          if attribute.match(/XX/).present?
            @sql_attributes.concat(add_number_var_to_attribute(attribute))
          else
            @sql_attributes << attribute
          end
        end
      end
      @sql_attributes
    end

    # The following are worker methods that parse and seperate the response 
    # that is sent back from google. 
    #
    # @return [Undefined]
    #

    def set_dimensions_and_metrics_hash_keys
      @items.each do |item|
        attri = item["attributes"]
        if attri["type"] == "METRIC"
          @metrics[attri["group"]] = []
        elsif attri["type"] == "DIMENSION"
          @dimensions[attri["group"]] = []
        end
      end
    end

    def set_custom_dimensions
      add_number_var_to_attribute("dimension", number_of_times: 200, regex: /$/, existing_array: @custom_variable_dimensions)
      add_number_var_to_attribute("customVarName", number_of_times: 50, regex: /$/, existing_array: @custom_variable_dimensions)
      add_number_var_to_attribute("customVarValue", number_of_times: 50, regex: /$/, existing_array: @custom_variable_dimensions)
    end

    def set_custom_metrics
      add_number_var_to_attribute("metric", number_of_times: 200, regex: /$/, existing_array: @custom_variable_metrics)
    end

    def aggregate_and_cast_dimensions
      @items.each do |item|
        attri = item["attributes"]
        dimension = item["id"].format_for_google
        if (attri["type"] == "DIMENSION") && (attri["status"] != "DEPRECATED")
          @dimensions[attri["group"]] << dimension
        end
      end
      @dimensions["Custom Variables or Columns"] = @custom_variable_dimensions
      @dimensions
    end

    def aggregate_and_cast_metrics
      @items.each do |item|
        attri = item["attributes"]
        metric = item["id"].format_for_google
        if (attri["type"] == "METRIC") && (attri["status"] != "DEPRECATED")
          if metric.match(/XX/).present?
            @metrics[attri["group"]] = add_number_var_to_attribute(metric, existing_array: metrics[attri["group"]])
          else
            @metrics[attri["group"]] << metric
          end
        end
      end
      @metrics["Custom Variables or Columns"] = @custom_variable_metrics
      @metrics
    end

end