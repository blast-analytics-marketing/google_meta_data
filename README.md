# GoogleMetaData

This gem is used to pull-in Google MetaData Dimensions and Metrics used across our applications and store them using Rails.cache.

## Installation

Add this line to your application's Gemfile, please note you will need to include the git option with the correct URL as this is not freely distributed:

    gem 'google_meta_data', git: "https://bitbucket.org/blast-analytics-marketing/google_meta_data.git"

And then execute:

    $ bundle

## Usage

Basic Usage:

The following methods will check the cache, if it is not available or expired, then it will create a new API call and return the result, as well as write the response to rails cache with an expiration time of 5 days. 

    To pull in Dimensions:
    GoogleDimensionsAndMetrics.fetch_dimensions

    To pull in Metrics:
    GoogleDimensionsAndMetrics.fetch_metrics

    To pull in Sql Attributes:
    * Sql attributes are metrics and dimensions that are neither a string nor integer. 
    GoogleDimensionsAndMetrics.fetch_sql_attributes

## Contributing

1. Fork it ( http://github.com/<my-github-username>/google_meta_data/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
