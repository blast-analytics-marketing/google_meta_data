# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'google_meta_data/version'

Gem::Specification.new do |spec|
  spec.name          = "google_meta_data"
  spec.version       = GoogleMetaData::VERSION
  spec.authors       = ["J. Shuster"]
  spec.email         = ["joshuagregoryshuster@gmail.com"]
  spec.summary       = %q{Cacheing of google metadata for inhouse projects}
  spec.description   = %q{Cacheing of google metadata for inhouse projects}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_runtime_dependency "rails"
  spec.add_runtime_dependency "httparty"
end
